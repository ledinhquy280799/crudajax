<?php

use App\Http\Controllers\AjaxCrudController;
use App\Http\Controllers\PaginationController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\AjaxCRUDImageController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('product-list', [ProductController::class, 'index']);
Route::get('product-list/{id}/edit', [ProductController::class, 'edit']);
Route::post('product-list/store', [ProductController::class, 'store']);
Route::get('product-list/delete/{id}', [ProductController::class, 'destroy']);

Route::get('ajax-crud-image-upload', [AjaxCRUDImageController::class, 'index']);
Route::post('add-update-book', [AjaxCRUDImageController::class, 'store']);
Route::post('edit-book', [AjaxCRUDImageController::class, 'edit']);
Route::post('delete-book', [AjaxCRUDImageController::class, 'destroy']);

Route::resource('ajax-crud', AjaxCrudController::class);

Route::post('ajax-crud/update', [AjaxCrudController::class, 'update'])->name('ajax-crud.update');

Route::get('ajax-crud/destroy/{id}', [AjaxCrudController::class, 'destroy']);

Route::group(['middleware' => ['web']], function() {
    Route::resource('post', PostController::class);
    Route::POST('addPost',[PostController::class, 'addPost']);
    Route::POST('editPost',[PostController::class, 'editPost']);
    Route::POST('deletePost',[PostController::class, 'deletePost']);
});

Route::get('/pagination', [PaginationController::class, 'index']);
Route::get('pagination/fetch_data', [PaginationController::class, 'fetch_data']);
