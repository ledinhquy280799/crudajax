{{--@if($image)--}}
{{--    <img id="preview" src="{{ ('public/product/'.$image) }}" alt="Preview" class="form-group hidden" width="100" height="100">--}}
{{--@else--}}
{{--    <img id="preview" src="https://via.placeholder.com/150" alt="Preview" class="form-group hidden" width="100" height="100">--}}
{{--@endif--}}

@if($image)
    <img src="{{ Storage::url($image) }}" height="75" width="75" alt="" />
@else
    <img src="https://www.riobeauty.co.uk/images/product_image_not_found.gif" alt="" height="75" width="75">
@endif
